import java.util.HashSet;
import java.util.Set;

public class Avion {
	private Set<String> placesDisponibles;
	private static final Avion INSTANCE = new Avion();
	
	public Avion() {
		placesDisponibles = new HashSet<String>();
		placesDisponibles.add("place1");
		placesDisponibles.add("place2");
		
	}
	
	public boolean acheterPlace(String place) {
		return this.placesDisponibles.remove(place);
	}

	public void venteTicket(String place) {
		Avion avion = new Avion();
		System.out.println(avion.acheterPlace(place));
	}
	
	public Avion getInstance() {
		return INSTANCE;
	}
	
}

