package com.inti.doa;

import java.util.List;

import com.inti.entities.Etudiant;

public interface IEtudiantDAO {
	List<Etudiant> findAll();
	Etudiant findById(Long id);
	void addEtudiant(Etudiant etudiant);
	void deleteEtudiant(Long id);
	void updateEtudiant (Etudiant etudiant);

}
