package com.inti.doa;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.inti.entities.Etudiant;
import com.inti.utils.Connexion;


public class EtudiantDAO implements IEtudiantDAO{

	@Override
	public List<Etudiant> findAll() {
		List<Etudiant> etudiants= new ArrayList<Etudiant>();
		try {
			Connexion.connect();
			String sql= "select * from etudiants";
			PreparedStatement statement = Connexion.conn.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				etudiants.add(new Etudiant(resultSet.getLong(1), resultSet.getNString(2) , resultSet.getNString(3)));
			}
			Connexion.disconnect();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return etudiants;
	}

	@Override
	public Etudiant findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addEtudiant(Etudiant etudiant) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteEtudiant(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateEtudiant(Etudiant etudiant) {
		// TODO Auto-generated method stub
		
	}

}
